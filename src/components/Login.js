import React from 'react';
import googleLogo from "../assets/google.svg";
import styles from './Login.module.css';

const Login = () => {
    return (
        <div className={styles.loginPage}>
            <div className={styles.loginCard}>
                <h2>Welcome to Botogram</h2>
                <div className={styles.button}>
                    <img src={googleLogo} alt="googleLogo"/>
                    Sign in with google
                </div>
            </div>
        </div>
    );
};

export default Login;